# 3D

Open source 3D stuff.

https://arstechnica.com/gadgets/2018/03/microsoft-announces-the-next-step-in-gaming-graphics-directx-raytracing/

## Engines

### [Ogre](https://www.ogre3d.org/)

- [ ] Try Ogre out
- License: MIT
- Source: [GitHub](https://github.com/OGRECave/ogre), [BitBucket](https://bitbucket.org/sinbad/ogre)
- Scripting: ?

### [Godot](https://godotengine.org/)

- [ ] Try Godot out
- License: MIT
- Source: [GitHub](https://github.com/godotengine/godot)
- Scripting: Python-like

### [Banshee](http://www.banshee3d.com/)

- [ ] Try Banshee out
- Source: [GitHub](https://github.com/BearishSun/BansheeEngine)
- License: GPL/LGPL
- Scripting: C# 6.0

### [Panda](https://www.panda3d.org/)

- [ ] Try Panda out
- Source: [GitHub](https://github.com/panda3d/panda3d)
- License: BSD
- Scripting: Python

### [Xenko](https://xenko.com/)

- [ ] Try Xenko out
- Source: [GitLab](https://git.xenko.com/xenko/Xenko-Runtime)
- License: EULA
- Scripting: C# 7.0

## Designers

### [Blender](https://www.blender.org/)

- [GitLab repository](http://hubelbauer.net/post/bloggo-blender)
- [Bloggo post](https://gitlab.com/TomasHubelbauer/bloggo-blender)

- [ ] Try Blender out
- Source: [Phabricator](https://developer.blender.org/diffusion/B/)
- License: GPL
- Scripting: Python

### [Wings](http://www.wings3d.com/)

- [ ] Try Wings out
- Source: [GitHub](https://github.com/dgud/wings)
- License: BSD
- Scripting: Erlang

### [OpenSCAD](http://www.openscad.org/)

- Source: [GitHub](https://github.com/openscad/openscad)
- License: GPL

### [FreeCAD](https://www.freecadweb.org/)

- Source: [GitHub](https://github.com/FreeCAD/FreeCAD)
- License: LGPL
